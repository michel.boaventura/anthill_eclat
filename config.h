/*config.h*/

#ifndef _CONFIG_H
#define _CONFIG_H

/*Inclusoes*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include <FilterDev/FilterDev.h>
#include <getTime.h>

using namespace std;

/*Definições*/
#define CONC_JOBS 4

#define ITEM_SIZE 150
#define LINE_SIZE 5000
#define MSG_SIZE 4000
#define T_MSG_SIZE MSG_SIZE*2
#define SIZE_FILE 300

#define EMPTY_SET -1
#define MAX_ITEMSETS 10000000
#define BLOCK_ITEMSETS 100000
#define EOI MAX_ITEMSETS+1000
#define FREE EOI+1
#define MSG EOI+2

#define KB 1024
#define MB 1024*KB
#define GB 1024*MB

#define MAX_MEM 900*MB

#define MIN_ARGS 5

typedef struct {
	int anticipating;
	double min_supp;
	double min_conf;
	char out_file[SIZE_FILE];
	char in_file[SIZE_FILE];
} Work;

typedef struct {
   int n_trans_local;
} reader_msg;

typedef struct {
	int type;
	int filter_id;
	char layout[T_MSG_SIZE];
} adder_msg;

typedef struct {
	int type;
	int filter_id;
	char id[T_MSG_SIZE];
	int sum;
	int mod;
	int size;
	int local_count;
} merger_msg;

typedef struct {
	int size;
	int local_count;
	char layout[T_MSG_SIZE];
} output_msg;

typedef struct {
	int index;
	int size;
	char item[ITEM_SIZE];
} item_msg;

typedef struct {
	int max_item;
	int id;
	int n_transaction;
} max_item_msg;

typedef struct {
	int id;
	int n_trans;
	int size;
	int transaction[MSG_SIZE];
} data_block_t;

#endif
