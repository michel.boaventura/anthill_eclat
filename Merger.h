/*Merger.h*/

#ifndef _MERGER_H
#define _MERGER_H

/*Inclusoes*/
#include <map>
#include <set>
#include <fstream>
#include <assert.h>
#include <ext/hash_map>
#include <sys/resource.h>

#include <memWrapper.h>
#include <config.h>

using namespace std;
using namespace __gnu_cxx;

/*Definições*/
#define INF 0
#define INIT 1
#define NOWNER -1
#define PARSE 100

#define SIZE_OF_ITEM "%s;FATAL;%s;instance %d of %d;Size of the frequent itemsets will be too large"

typedef struct {
   timeval start, stop;
   int id;
   int size;
   int count;
   int* layout;
   int* list;
   int is_freq;
} itemset;

typedef struct {
   itemset* its;
   int size;
} table;

struct eqstr{
	bool operator()(const char* s1, const char* s2) const{
		return strcmp(s1, s2) == 0;
	}
};

/*Variaveis Globais*/
table tab;
int my_rank, n_instances;
int n_adder_instances,n_reader_instances;
long int total_allocated;
hash_map<const char*, int , hash<const char*>, eqstr> layout_map_id;
hash_map<const char*, int , hash<const char*>, eqstr> pendencies_table;

/*Prototipos*/
extern "C"{
	int initFilter(void* work, int size);
	int processFilter(void* work, int size);
	int finalizeFilter(void);
}

void constructLists(int *n_transactions_local);
int contains(itemset I, int k);
void createLayout(itemset a,itemset b,itemset *result);
void createList(itemset a,itemset b,itemset *result);
int getId(char *layout,int *n_message);
void initializeTable(int max_items);
itemset meet(itemset a, itemset b);
void print_itemset(FILE* ff, itemset it);
void receiveReaderMessage(int *n_transactions_local,int *max_item);
void reallocTable(int size);
void sendMessage(merger_msg* out_msg, int type,int index,int count,int* n_message,int *pendency,int *position);
static int sort_layout(const void *a, const void *b);
void verifyMaxItemset(int n_itemsets);

#endif
