#include <my_env.h>

int lsize() {
  char *env = getenv("LINE_SIZE");
  return env ? atoi(env) : LINE_SIZE;
}

int msize() {
  char *env = getenv("MSG_SIZE");
  return env ? atoi(env) : MSG_SIZE;
}
