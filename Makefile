#
# Site options -=- modify to match your envirioment
#

#ANTHILL_PREFIX ?= /usr/
#ANTHILL_INCLUDE ?= ${ANTHILL_PREFIX}/include/anthill
#ANTHILL_LIB ?= ${ANTHILL_PREFIX}/lib/

#PREFIX ?= /usr/lib/anthill/eclat-1.0/

#
# Common options and rules -=- you should not edit anything bellow!!!
#

CC = g++ -Wall -g -fPIC -Wno-deprecated -Wno-write-strings
#CFLAGS = -I${ANTHILL_INCLUDE}/ -I${ANTHILL_INCLUDE}/FilterDev/ -I${ANTHILL_INCLUDE}/FilterData/ -I${ANTHILL_INCLUDE}/AbstractDataTypes/
CFLAGS = -I.
CLIBS = -lpvm3 -lexpat -ldl -lds
#CLIBS = -lpvm3 -lexpat -ldl -L${ANTHILL_LIB} -lds

OBJS = main Merger.so Adder.so labelfunc.so Reader.so Aggregator.so

all:target

target: ${OBJS}

Reader.so: Reader.cc Reader.h config.h memWrapper.cc memWrapper.h my_env.c my_env.h
	${CC} ${CFLAGS} ${CLIBS} -shared -o Reader.so Reader.cc memWrapper.cc getTime.cc my_env.c

Aggregator.so: Aggregator.cc Aggregator.h config.h memWrapper.cc memWrapper.h my_env.c my_env.h
	${CC} ${CFLAGS} ${CLIBS} -shared -o Aggregator.so Aggregator.cc memWrapper.cc getTime.cc my_env.c

Adder.so: Adder.cc Adder.h config.h memWrapper.cc memWrapper.h my_env.c my_env.h
	${CC} ${CFLAGS} ${CLIBS} -shared -o Adder.so Adder.cc memWrapper.cc getTime.cc my_env.c

Merger.so: Merger.cc Merger.h config.h memWrapper.cc memWrapper.h my_env.c my_env.h
	${CC} ${CFLAGS} ${CLIBS} -shared -o Merger.so Merger.cc memWrapper.cc getTime.cc my_env.c

labelfunc.so: labelfunc.c config.h my_env.c my_env.h
	${CC} ${CFLAGS} ${CLIBS} -shared -o labelfunc.so labelfunc.c getTime.cc my_env.c

main: main.cc config.h
	${CC} ${CFLAGS} ${CLIBS} main.cc getTime.cc -o main

.PHONY: clean install test

clean:
	rm -f *.o *~ Merger.so Adder.so labelfunc.so Aggregator.so Reader.so main ADDER* MERGER* READER* AGGREG* TEMPO trace*

test: target
	./initScript  -t 0 -s 0.01 -c 90 -i teste -o out3

install: ${OBJS}
	mkdir -pv ${PREFIX}
	cp -v ${OBJS} initScript conf.xml ${PREFIX}

deb: ${OBJS}
	equivs-build DEBIAN/control 
