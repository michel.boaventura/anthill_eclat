/*Aggregator.cc*/

/*Inclusoes*/
#include <Aggregator.h>

InputPortHandler in_port_reader_agg_NTRANS;
InputPortHandler in_port_reader_aggregator;
InputPortHandler in_port_adder_aggregator;
OutputPortHandler out_port_aggregator_reader;

int initFilter(void* work, int size) {
	my_rank = dsGetMyRank();
	n_instances = dsGetTotalInstances();
	
	//pega paramametros do usuario
	Work args = (*(Work *)work);
	strcpy(out_file,strdup(args.out_file));
	min_conf = args.min_conf/(double)100;

	//configura portas de comunicacao
	in_port_reader_agg_NTRANS = dsGetInputPortByName("in_reader_agg_NTRANS");
	in_port_reader_aggregator = dsGetInputPortByName("in_reader_aggregator");
	in_port_adder_aggregator  = dsGetInputPortByName("in_adder_aggregator");
	out_port_aggregator_reader = dsGetOutputPortByName("out_aggregator_reader");

	//inicializa��o de variavel global
	total_allocated = 0;
	
	return 1;
}

int processFilter(void* work, int size){
	//declara��o de variaveis
	aggSet itemSet;
	int n_transactions = 0;
	int n_freq_itemsets = 0;
	map <set<int>, double> freq_map;
		
	itemSet.its = (itemset_agg*) mymalloc(sizeof(itemset_agg)*BLOCK_ITEMSETS);
	total_allocated+=getAllocatedMem(itemSet.its);
	itemSet.size = BLOCK_ITEMSETS;
	reader_msg* r_msg = (reader_msg*) mymalloc(sizeof(reader_msg));
	total_allocated+=getAllocatedMem(r_msg);
	output_msg* in_msg = (output_msg*) mymalloc(sizeof(output_msg));
	total_allocated+=getAllocatedMem(in_msg);
	
	//verifica se o n�mero de inst�ncias � um
	verifyNumInstances();
	
	//verifica se o min_conf possue valor coerente
	verifyMinConf(min_conf);
	
	//fun��o responsavel por receber os itemsets do filtro leitor, dar um id unico e global para cada
	//itemset recebido e informar ao filtro leitor os ids atribuidos a cada itemset
	attribId();

	//recebe o numero total de transa��es lidas por todas as instancias de reader
	while (dsReadBuffer(in_port_reader_agg_NTRANS, r_msg, sizeof(reader_msg)) != EOW)
		n_transactions+=r_msg->n_trans_local;
	total_allocated-=getAllocatedMem(r_msg);
	myfree(r_msg);
	
	//copia as informa��es contidas na mensagem para um armazenamento temporario
	while (dsReadBuffer(in_port_adder_aggregator, in_msg, sizeof(output_msg)) != EOW) {
			reallocAggSet(&itemSet,n_freq_itemsets);
			copyInformations(itemSet,n_freq_itemsets,in_msg);
			n_freq_itemsets++;
	}
	total_allocated-=getAllocatedMem(in_msg);
	myfree(in_msg);

	//calcula o suporte global de cada itemset frequente
	for(int i=0;i<n_freq_itemsets;i++) {
		set<int> key;
		for(int j=0;j<itemSet.its[i].size;j++) 
			key.insert(itemSet.its[i].layout[j]);
		freq_map[key] = (itemSet.its[i].count)/(double)(n_transactions);
		key.clear();
	}

	//gera e imprime as regras de associa��o,para os itemsets frequentes encontrados,no arquivo de saida
	generateRules(n_freq_itemsets,itemSet,n_transactions,freq_map);

	total_allocated-=getAllocatedMem(itemSet.its);
	myfree(itemSet.its);
	
	return 1;
}

int finalizeFilter(void){
	//releaseInputPort(in_port_reader_aggregator);
	//releaseInputPort(in_port_merger_aggregator);
	return 1;
}

//metodoo responsavel por receber os itemsets do filtro leitor, dar um id unico e global para cada
void attribId(){
	char *item;
	int n_itemsets = 0;
	item_msg* items = (item_msg*) mymalloc(sizeof(item_msg));
	total_allocated+=getAllocatedMem(items);
	
	//cria ids unicos e globais para cada itemset enviado pelo filtro leitor
	while (dsReadBuffer(in_port_reader_aggregator, items, sizeof(item_msg)) != EOW){
		item = (char*) mymalloc(sizeof(char)*(items->size+1));
		total_allocated+=getAllocatedMem(item);
		
		strcpy(item,items->item);
		if(!item_map_id[item]){
			n_itemsets++;
			item_map_id[item] = n_itemsets;
		}
	}
	
	//envia para todas as instancias do filtro leitor os ids atribuidos a cada itemset
	hash_map<const char*, int, hash<const char*>, eqstr>::iterator i;
	for(i=item_map_id.begin();i!=item_map_id.end();i++) {
		items->index = i->second;
		items->size = strlen(i->first);
		strcpy(items->item,i->first);
		dsWriteBuffer(out_port_aggregator_reader,items, sizeof(item_msg));
	}
	
	//encerra comunica��o com o Reader
	dsCloseOutputPort(out_port_aggregator_reader);
	
	total_allocated-=getAllocatedMem(items);
	myfree(items);
}

//copia as informa��es contidas na mensagem para um local de armazenamento temporario
void copyInformations(aggSet itemSet,int n_freq_itemsets,output_msg* in_msg){	
	itemSet.its[n_freq_itemsets].count = in_msg->local_count;
	itemSet.its[n_freq_itemsets].size = 0;
	itemSet.its[n_freq_itemsets].layout = (int*) mymalloc(sizeof(int)*in_msg->size);

	if(itemSet.its[n_freq_itemsets].layout == NULL) {
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,ALOC_ERROR,time_s,"AGGREGATOR",my_rank);
		dsExit(fatalMessage);
	}
	
	char* item = strtok(in_msg->layout, " ");
	while(item!=NULL) {
		itemSet.its[n_freq_itemsets].layout[itemSet.its[n_freq_itemsets].size++] = atoi(item);
		item = strtok(NULL, " ");
	}
}

int does_A_contains_K(itemset_agg a, int k) {
	for(int i=0;i<a.size;i++) if(a.layout[i]==k) return 1;
	return(0);
}

//gera e imprime as regras de associa��o,para os itemsets frequentes encontrados,no arquivo de saida
void generateRules(int n_freq_itemsets,aggSet itemSet,int n_transactions,map <set<int>, double> freq_map){
	int i,j;
	FILE* file;
	openFile(&file);
	
	for(i=0;i<n_freq_itemsets;i++) {
		for(j=0;j<n_freq_itemsets;j++) {
			if(is_A_superset_OF_B(itemSet.its[i], itemSet.its[j])) {
				print_rule(file, itemSet.its[j], itemSet.its[i], n_transactions, freq_map);
			}
		}
	}
	
	fclose(file);
}

//fun��o responsavel por pegar o itemset correspondente ao id passado como parametro
char *getItemset(int id){
	hash_map<const char*, int, hash<const char*>, eqstr>::iterator p;
	for(p=item_map_id.begin();p!=item_map_id.end();p++) {
		if(item_map_id[(*p).first] == id)
			return ((char*) (*p).first);
	}
	
	char *errorMessage = (char *)calloc(1000, sizeof(char));
	time_t now;
	char *time_s = getTime(now);
	sprintf(errorMessage,ID_ERROR,time_s,"AGGREGATOR",my_rank,id);
	dsExit(errorMessage);
	
	return NULL;
}

int is_A_superset_OF_B(itemset_agg a, itemset_agg b) {
	if(a.size <= b.size) return 0;
	for(int i=0;i<b.size;i++) {
		if(!does_A_contains_K(a, b.layout[i])) return 0;
	}
	return(1);
}

//metodo responsavel por abrir o arquivo de saida
void openFile(FILE **file){
	sprintf(out_file,"%s", out_file);
	*file = fopen(out_file, "w");
	
	if (*file == NULL) {
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,FILE_ERROR,time_s,"AGGREGATOR",out_file);
		dsExit(fatalMessage);
	}
}

//fun��o respons�vel por imprimir no arquivo de sa�da todas as regras geradas
int print_rule(FILE* file, itemset_agg a, itemset_agg b,int n_transactions,map <set<int>, double> freq_map) {
	if((b.count)/(double)(a.count) >= min_conf) {
		set<int> key;
		for(int i=0;i<a.size;i++) key.insert(a.layout[i]);
		double L = freq_map[key];
		key.clear();

		for(int i=0;i<b.size;i++) {
			if(!does_A_contains_K(a, b.layout[i])) key.insert(b.layout[i]);
		}
		double R = freq_map[key];

		double lift=((b.count)/(double)(a.count))/(double)R;
		double leverage=((b.count)/(double)(n_transactions))-(L*R);
		double conviction=(1-R)/(double)(1-((b.count)/(double)(a.count)));

		fprintf(file, "%lf ", lift);
		fprintf(file, "%lf ", leverage);
		fprintf(file, "%lf ", conviction);
		fprintf(file, "%lf ", 100*((b.count)/(double)(a.count)));
		fprintf(file, "%lf ", 100*((b.count)/(double)(n_transactions)));
		for(int i=0;i<a.size;i++) {
			fprintf(file, "%s", getItemset(a.layout[i]));
			if(i<(a.size-1)) fprintf(file, ";");
		}
		fprintf(file, "=>");
		for(int i=0;i<b.size;i++) {
			if(!does_A_contains_K(a, b.layout[i])) {
				fprintf(file, "%s", getItemset(b.layout[i]));
				fprintf(file,";");
			}
		}
		fprintf(file, "\n");
	}
	return(1);
}

//m�todo respons�vel por alocar espa�o de mem�ria suficiente para a estrutura aggset
void reallocAggSet(aggSet *itemSet,int size){
	//realoca espa�o para a estrutura itemSet
	if(size == itemSet->size){
		total_allocated-=getAllocatedMem(itemSet->its);
		itemSet->its = (itemset_agg*) myrealloc(itemSet->its,sizeof(aggSet) * (itemSet->size + BLOCK_ITEMSETS));
		total_allocated+=getAllocatedMem(itemSet->its);
		
		itemSet->size+=BLOCK_ITEMSETS;
	}
	
	//verifica se a memoria alocada est� dentro dos limites estabelecidos
	verifyMemory(total_allocated,"AGGREGATOR",my_rank,n_instances);
}

void verifyMinConf(double min_conf){
	//verifica se a confian�a m�nima � coerente
	if(min_conf <= 0.0) {
		char *warningMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(warningMessage,WRG_MIN_CONF,time_s,"AGGREGATOR",my_rank);
		printf("%s\n",warningMessage);
		min_conf = 0.01;
	}
	
	//verifica se a confian�a m�xima � coerente
	if(min_conf > 1.0){
		char *warningMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(warningMessage,WRG_MAX_CONF,time_s,"AGGREGATOR",my_rank);
		printf("%s\n",warningMessage);
		min_conf = 1.0;
	}

}

void verifyNumInstances(){
	if(n_instances != 1) {
		char *fatalMessage = (char *)calloc(1000, sizeof(char));
		time_t now;
		char *time_s = getTime(now);
		sprintf(fatalMessage,INST_ERROR,time_s,"AGGREGATOR",my_rank);
		dsExit(fatalMessage);
	}
}
